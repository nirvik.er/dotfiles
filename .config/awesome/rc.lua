pcall(require, "luarocks.loader")
require("awful.autofocus")

-- MODULES
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local hotkeys_popup = require("awful.hotkeys_popup")

-- NEED ERROR LOG

-- [THEME FILE]
beautiful.init("~/.config/awesome/theme.lua")

-- [CONFIGURATION]
local terminal          = "alacritty"
local editor            = "emacsclient --alternate-editor=emacs --create-frame"
local editor_cmd        = editor
local gui_editor        = "vscodium"
local browser           = "brave"
local browser_incognito = "brave --incognito"
local file_manager      = "pcmanfm"
local music_player      = "clementine"
local screen_locker     = "i3lock-fancy"
local screenshot        = "flameshot gui"
local desktop_launcher  = "rofi -show drun"
local bin_launcher      = "rofi -show run"
local rss_feed          = "alacritty -e newsboat"
local browser_news      = browser .. " news.google.com english.onlinekhabar.com"
local wallpaper_setter  = "feh --bg-fill --randomize /home/quiescent/Customisations/wallpapers"
local mode              = "normal" -- zen, meditation
modkey = "Mod4"

awful.layout.layouts = {awful.layout.suit.max, awful.layout.suit.tile, awful.layout.suit.spiral}

-- [AUTOSTART]
awful.spawn(wallpaper_setter)
awful.spawn("parcellite")
awful.spawn("/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1")
awful.spawn("mate-power-manager")
awful.spawn("picom")
awful.spawn("nm-applet")
awful.spawn("emacs --daemon")
-- awful.spawn("volumeicon")
-- awful.spawn("blueman-manager")

-- [WIBAR]
local taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t) t:view_only() end),
    awful.button({modkey}, 1, function(t) if client.focus then client.focus:move_to_tag(t) end end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({modkey}, 3, function(t) if client.focus then client.focus:toggle_tag(t) end end),
    awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c) if c == client.focus then c.minimized = true else c:emit_signal("request::activate", "tasklist", { raise = true }) end end),
    awful.button({}, 3, function() awful.menu.client_list({theme = {width = 50}}) end),
    awful.button({}, 4, function() awful.client.focus.byidx(1) end),
    awful.button({}, 5, function() awful.client.focus.byidx(-1) end)
)

awful.screen.connect_for_each_screen(function(s)
    -- Tags
    awful.tag({"PRODUCTIVITY", "EDIT", "FILE", "PERSONAL", "SYSTEM", "INTERNET", "OTHER"}, s, awful.layout.layouts[1])

    s.layout_box = awful.widget.layoutbox(s)
    s.layout_box:buttons(gears.table.join(awful.button({}, 1, function()
        awful.layout.inc(1)
    end), awful.button({}, 3, function()
        awful.layout.inc(-1)
    end), awful.button({}, 4, function()
        awful.layout.inc(1)
    end), awful.button({}, 5, function()
        awful.layout.inc(-1)
    end)))

    -- Create a taglist widget
    s.tag_list = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- [WIBOX]
    s.wibox = awful.wibar({
        position = "top",
        screen = s
        -- opacity = 0.8
    })

    s.textclock = wibox.widget.textclock("%b %d %I:%M %p", 60)
    s.tasklist = awful.widget.tasklist {
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    s.wibox:setup{
        layout = wibox.layout.align.horizontal,
        {
            layout = wibox.layout.fixed.horizontal,
            launcher,
            s.tag_list,
            s.layout_box,
        },
        s.tasklist,
        {
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            s.textclock
        }
    }
end)

-- [MOUSE BINDINGS]
root.buttons(gears.table.join(awful.button({}, 3, function()
    main_menu:toggle()
end), awful.button({}, 4, awful.tag.viewnext), awful.button({}, 5, awful.tag.viewprev)))

-- [KEY BINDINGS]
globalkeys = gears.table.join(
    -- GROUP - AWESOME
    awful.key({modkey}, "q", awesome.restart, { description = "Reload Awesome", group = "Awesome"}),
    awful.key({modkey, "Shift"}, "q", awesome.quit, { description = "Quit Awesome", group = "Awesome"}),
    awful.key({modkey}, "w", function()  awful.spawn(editor .. " /home/quiescent/.config/awesome/rc.lua") end, { description = "Edit Awesome Configuration", group = "Awesome"}),
    awful.key({modkey, "Shift"}, "w", hotkeys_popup.show_help, { description = "Show Help", group = "Awesome"}),

    -- GROUP - CLIENT
    awful.key({modkey}, "j", function() awful.client.focus.byidx(1) end, { description = "Focus Next", group = "Client"}),
    awful.key({modkey, "Shift"}, "j", function() awful.client.swap.byidx(1) end, {description = "Swap With Next Client", group = "Client"}),
    awful.key({modkey}, "k", function() awful.client.focus.byidx(-1) end, {description = "Focus Previous", group = "Client"}),
    awful.key({modkey, "Shift"}, "k", function() awful.client.swap.byidx(-1) end, {description = "Swap With Previous Client", group = "Client"}),
    awful.key({modkey}, "u", awful.client.urgent.jumpto, {description = "Urgent Client", group = "Client"}),

    -- GROUP - LAUNCHER
    awful.key({modkey}, "Return", function() awful.spawn(terminal) end, {description = "Default Terminal", group = "Launcher"}),
    awful.key({modkey, "Shift"}, "Return", function() awful.spawn(terminal .. " -e su") end, {description = "Terminal Root", group = "Launcher"}),
    awful.key({modkey}, "s", function() awful.spawn(screen_locker) end, {description = "Screenlocker", group = "Launcher"}),
    awful.key({modkey}, "r", function() awful.spawn(desktop_launcher) end, {description = "Run App Launcher", group = "Launcher"}),
    awful.key({modkey}, "b", function() awful.spawn(browser) end, {description = "Default Browser", group = "Launcher"}),
    awful.key({modkey, "Shift"}, "b", function() awful.spawn(browser_incognito) end, {description = "Incognito Browser", group = "Launcher"}),
    awful.key({modkey}, "e", function() awful.spawn(editor) end, {description = "Editor", group = "Launcher"}),
    awful.key({modkey, "Shift"}, "e", function() awful.spawn(editor .. " /home/quiescent/.config/doom/config.el") end, {description = "Doom Config", group = "Launcher"}),
    awful.key({modkey}, "a", function() awful.spawn("anki") end, {description = "Anki", group = "Launcher"}),
    awful.key({modkey}, "g", function() awful.spawn("gimp") end, {description = "GIMP", group = "Launcher"}),
    awful.key({modkey}, "v", function() awful.spawn(gui_editor) end, {description = "Graphical Editor", group = "Launcher"}),
    awful.key({modkey}, "m", function() awful.spawn(file_manager) end, {description = "File Manager", group = "Launcher"}),
    awful.key({modkey, "Shift"}, "m", function() awful.spawn(music_player) end, {description = "Music Player", group = "Launcher"}),
    awful.key({modkey}, "n", function(c) awful.spawn(rss_feed) end, {description = "RSS Feed", group = "Launcher"}),
    awful.key({modkey, "Shift"}, "n", function(c) awful.spawn(browser_news) end, {description = "Onlinekhabar and Google News", group = "Launcher"}),

    -- GROUP - LAYOUT
    awful.key({modkey}, "l", function() awful.tag.incmwfact(0.05) end, {description = "Increase Master Width Factor", group = "Layout"}),
    awful.key({modkey}, "h", function() awful.tag.incmwfact(-0.05) end, {description = "Decrease Master Width Factor", group = "Layout"}),
    awful.key({modkey, "Control"}, "h", function() awful.tag.incncol(1, nil, true) end, {description = "Increase The Number of Columns", group = "Layout"}),
    awful.key({modkey, "Control"}, "l", function() awful.tag.incncol(-1, nil, true) end, {description = "Decrease The Number of Columns", group = "Layout"}),
    awful.key({modkey}, "space", function() awful.layout.inc(1) end, {description = "Next Layout", group = "Layout"}),

    -- GROUP - ACTION
    awful.key({}, "XF86AudioRaiseVolume", function() awful.spawn("amixer set Master 5%+") end, {description = "Increase Volume 5%", group = "Action"}),
    awful.key({}, "XF86AudioLowerVolume", function() awful.spawn("amixer set Master 5%-") end, {description = "Decrease Volume 5%", group = "Action"}),
    awful.key({}, "XF86AudioMute", function() awful.spawn("amixer set Master toggle") end, {description = "Toggle Volume", group = "Action"}),
    awful.key({}, "Print", function() awful.spawn("flameshot gui") end, {description = "Flameshot Rectangular Region", group = "Action"}),
    awful.key({"Shift"}, "Print", function() awful.spawn("flameshot launcher") end, {description = "Flameshot Launcher", group = "Action"})
)
clientkeys = gears.table.join(
    awful.key({modkey}, "f", function(c) c.fullscreen = not c.fullscreen c:raise() end, {description = "Toggle Fullscreen", group = "Client"}),
    awful.key({modkey, "Shift"}, "f", function(c) c.ontop = not c.ontop end, {description = "Always On Top", group = "Client"}),
    awful.key({modkey}, "c", function() awful.spawn("xkill") end, {description = "Kill", group = "Client"}),
    awful.key({modkey, "Shift"}, "c", function(c) c:kill() end, {description = "Close", group = "Client"}),
    awful.key({modkey}, "t", awful.client.floating.toggle, {description = "Toggle Floating", group = "Client"}),
    awful.key({modkey}, "o", function(c) c:move_to_screen() end, {description = "Move to Screen", group = "Client"})
)

for i = 1, 7 do
    -- GROUP - TAG
    globalkeys = gears.table.join(globalkeys, awful.key({modkey}, "#" .. i + 9, function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
            tag:view_only()
        end
    end, {
        description = "View Tag #" .. i,
        group = "Tag - View"
    }), awful.key({modkey, "Shift"}, "#" .. i + 9, function()
        if client.focus then
            local tag = client.focus.screen.tags[i]
            if tag then
                client.focus:move_to_tag(tag)
            end
        end
    end, {
        description = "Move Focused Client to Tag #" .. i,
        group = "Tag - Move"
    }))
end

clientbuttons = gears.table.join(awful.button({}, 1, function(c)
    c:emit_signal("request::activate", "mouse_click", {
        raise = true
    })
end), awful.button({modkey}, 1, function(c)
    c:emit_signal("request::activate", "mouse_click", {
        raise = true
    })
    awful.mouse.client.move(c)
end), awful.button({modkey}, 3, function(c)
    c:emit_signal("request::activate", "mouse_click", {
        raise = true
    })
    awful.mouse.client.resize(c)
end))

-- [KEYS]
root.keys(globalkeys)

-- [RULES]
awful.rules.rules = {{
    rule = {},
    properties = {
        border_width = beautiful.border_width,
        border_color = beautiful.border_normal,
        focus = awful.client.focus.filter,
        raise = true,
        keys = clientkeys,
        buttons = clientbuttons,
        screen = awful.screen.preferred,
        placement = awful.placement.no_overlap + awful.placement.no_offscreen,
        maximized = false
    }
}, {
    rule_any = {
        class = {"Brave", "fluent-reader", "Librewolf", "Element"}
    },
    properties = {
        screen = 1,
        tag = "INTERNET"
    }
}, {
    rule_any = {
        class = {"Pcmanfm", "Font-manager", "vlc", "mpv", "Clementine"}
    },
    properties = {
        screen = 1,
        tag = "FILE"
    }
}, {
    rule_any = {
        class = {"Anki", "com.github.babluboy.bookworm"}
    },
    properties = {
        screen = 1,
        tag = "PRODUCTIVITY"
    }
}, {
    rule_any = {
        class = {"Portmaster", "qBittorrent", "kalarm"}
    },
    properties = {
        screen = 1,
        tag = "OTHER"
    }
}, {
    rule_any = {
        class = {"Portmaster"}
    },
    properties = {
        screen = 1,
        minimised = true
    }
}, {
    rule_any = {
        class = {"Rednotebook"}
    },
    properties = {
        screen = 1,
        tag = "PERSONAL"
    }
}, {
    rule_any = {
        class = {"Timeshift", "Nwg-look"}
    },
    properties = {
        screen = 1,
        tag = "SYSTEM"
    }
}
}

-- [SIGNAL]
client.connect_signal("manage", function(c)
    if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)

-- [BORDER]
client.connect_signal("focus", function(c)
    c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
    c.border_color = beautiful.border_normal
end)
