local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local beautiful = require("beautiful")

local gfs = require("gears.filesystem")
local gears = require("gears")
local themes_path = "~/.config/awesome/svg-icons"

-- [NORD]
colorBG = "#2E3440"
colorFG = "#D8DEE9"
-- Normal
color00 = "#3B4252" -- Black
color01 = "#BF616A" -- Red
color02 = "#A3BE8C" -- Green
color03 = "#EBCB8B" -- Yellow
color04 = "#81A1C1" -- Blue
color05 = "#B48EAD" -- Magenta
color06 = "#88C0D0" -- Cyan
color07 = "#E5E9F0" -- White
-- Bright
color08 = "#434C5E" -- Black
color09 = "#BF616A" -- Red
color10 = "#A3BE8C" -- Green
color11 = "#A3BE8C" -- Yellow
color12 = "#81A1C1" -- Blue
color13 = "#B48EAD" -- Magenta
color14 = "#8FBCBB" -- Cyan
color15 = "#ECEFF4" -- White

local theme = {}

theme.font = "JetBrainsMono Bold 9"

theme.bg_normal = colorBG
theme.bg_focus = color04
theme.bg_urgent = color01
theme.bg_minimize = color08
theme.bg_systray = colorBG
theme.fg_normal = colorFG
theme.fg_focus = colorBG
theme.fg_urgent = colorBG
theme.fg_minimize = colorFG

theme.useless_gap = dpi(4)
theme.border_width = dpi(2)
theme.border_normal = color08
theme.border_focus = color01
theme.border_marked = color03

-- [TASKLIST]
theme.tasklist_bg_normal = colorBG
theme.tasklist_bg_focus = color04
theme.tasklist_bg_urgent = color01
theme.tasklist_fg_normal = colorFG
theme.tasklist_fg_focus = colorBG
theme.tasklist_fg_urgent = colorBG

-- [TAGLIST]
theme.taglist_bg_normal = colorBG
theme.taglist_bg_focus = color04
theme.taglist_bg_empty = colorBG
theme.taglist_bg_volatile = color01
theme.taglist_bg_occupied = colorBG

theme.taglist_fg_normal = colorFG
theme.taglist_fg_current = colorBG
theme.taglist_fg_occupied = colorFG
theme.taglist_fg_volatile = colorBG
theme.taglist_fg_empty = color00
theme.tasklist_disable_icon = true

-- [TOOLTIP]
theme.tooltip_opacity = 1

-- [NOTIFICATION]
theme.notification_width = dpi(400)
theme.notification_height = dpi(200)
theme.notification_margin = dpi(0)
theme.notification_border_color = colorBG
theme.notification_border_width = dpi(0)
theme.notification_opacity = 0.8
theme.notification_shape = function(cr)
    gears.shape.rounded_rect(cr, 400, 200, 10)
end

-- [HOTKEYS]
theme.hotkeys_bg = colorBG
theme.hotkeys_fg = colorFG
theme.hotkeys_border_width = dpi(2)
theme.hotkeys_border_color = color01
theme.hotkeys_opacity = 1
theme.hotkeys_modifiers_fg = color11
theme.hotkeys_label_bg = color04
theme.hotkeys_label_fg = colorBG
theme.hotkeys_font = "Jetbrains Mono 10"
theme.hotkeys_description_font = "Jetbrains Mono 9"
theme.hotkeys_shape = function(cr,width,height)
    gears.shape.rounded_rect(cr, width, height, 8)
end

-- [LAYOUTS]
theme.layout_floating = themes_path .. "/layouts/float.svg"
theme.layout_magnifier = themes_path .. "/layouts/magnifier.svg"
theme.layout_max = themes_path .. "/layouts/fullscreen.svg"
theme.layout_tile = themes_path .. "/layouts/tile.svg"
theme.layout_spiral = themes_path .. "/layouts/spiral.svg"

-- [AWESOME ICONS]
theme.awesome_icon = themes_path .. "/identified/archlinux-blue.svg"

-- [ICONS]
theme.icon_theme = "/usr/share/icons/Papirus-Dark/"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
