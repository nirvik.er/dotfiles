## ZSH_NEWUSER_INSTALL 
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
bindkey -v

## EXTRACTION
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

## COMPINSTALL
zstyle :compinstall filename '/home/agent/.zshrc'
autoload -Uz compinit
compinit

## STARSHIP PROMPT
eval "$(starship init zsh)"

## ALIASES
# Replacement 
alias ls="exa --icons -a "
alias ll="exa --icons -la"
alias cat="bat"
alias vim="nvim"
alias emacs="emacsclient"

# New
alias update-mirrors="echo 'sudo rankmirrors -n 10 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist'"
alias cl="clear"

# Tags
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias btop="btop -lc"
alias df="df -h"
alias mkdir='mkdir -pv'

## SOURCES & EXPORTS

export MANPAGER="sh -c 'col -bx | bat -l man -p'"

source ~/Downloads/scripts/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/Downloads/scripts/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

## AUTOSTART WITH SHELL 
colorscript -r 
motivate 
